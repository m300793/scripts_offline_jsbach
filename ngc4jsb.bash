#!/bin/bash -xvf
#SBATCH --account=mh0731
#SBATCH --job-name=jsb_forcing
#SBATCH --partition=compute
#SBATCH --nodes=1
#SBATCH --mem=450GB
#SBATCH --time=08:00:00
#SBATCH --output=ngc4jsb.%j.log
#SBATCH --error=ngc4jsb.%j.log
set -e

. /usr/share/Modules/init/bash

# -----------------------------------------------------------------------------------------------------
# This program extracts climate data from NextGems Cycle X simulations and creates JSB4 forcing at
# different spatial and temporal resolutions.
# -----------------------------------------------------------------------------------------------------
#
# For zoom level 10 applications, the easiest approach is to get an interactive node with large memory
# using `salloc --x11 -p interactive -A bm1236 --mem=450GB` and run the script there or use an compute node
# with sufficient memory. 450MB and 8h runtime are sufficient for 1 year of R2B9 forcing data generation

CONTACT="junhong.lee@mpimet.mpg.de"
INSTITUTE="Max Planck Institute for Meteorology"

if [ "$1" == "-h" -o "$1" == "--help" -o ${#@} -eq 0 ] ; then
  echo ""
  echo "Usage: `basename $0` [-h] [-g icon_grid_????_R??B??_G.nc] [-s P1D] [-y Y1-Y2] dataset"
  echo "with:"
  echo "  -d, --data   choose dataset: [ngc3028]"
  echo "optionally provide:"
  echo "  -g, --grid   target grid file, e.g. icon_grid_0043_R02B04_G.nc"
  echo "  -s, --step   target time step [P1D, PT3H, PT30M]"
  echo "  -y, --year   target time step [2021-2024]"
  echo ""
  exit 0
fi

PARACHECK=true
DATA="missing"
GRID="/pool/data/ICON/grids/public/mpim/0043/icon_grid_0043_R02B04_G.nc"
STEP="P1D"
YEAR="2021-2024"

# Update datset and optionally defaults from command line
while $PARACHECK; do
  case $1 in
    -d | --data ) DATA=$2 && shift 2;;
    -g | --grid ) GRID=$(ls $2) && shift 2;;
    -s | --step ) STEP=$2 && shift 2;;
    -y | --year ) YEAR=$2 && shift 2;;
    *           ) PARACHECK=false;;
  esac
done

[[ "$DATA" == "missing"   ]] && echo "ERROR: Provide dataset name with -d option" && exit 1

# Catch not implemented time step request
case $STEP in
  PT3H ) RW_TIME="-settaxis,YYYY-MM-01,00:00:00,3hour" ;;
  P1D  ) RW_TIME="-settaxis,YYYY-MM-01,00:00:00,1day" ;;
  *    ) echo "Error: Timestep $STEP not available for $DATA" && exit 1;;
esac

# Load hms tools
module use /work/k20200/k202134/hsm-tools/outtake/module
module load hsm-tools/unstable
module load nco

# Currently a specific cdo version is required
module use /home/m/m221078/etc/Modules
module add netcdf-dev
module unload cdo
module add cdo-dev
which cdo
cdo="cdo -s -f nc4c --reduce_dim --no_history"

# Select zoom level
case $GRID in
  *R02B09* ) ZOOM=10 ;;
  *R02B06* ) ZOOM=8  ;;
  *R02B04* ) ZOOM=7  ;;
  * )        ZOOM=10 ;;
esac

# Create target directory
GTAG="$(basename $GRID | cut -d"_" -f4)/$(basename $GRID | cut -d"_" -f3)"
DTRG=$(echo "${GTAG}/$DATA" | tr [a-z] [A-Z])
test -d $DTRG || mkdir -p $DTRG

# -----------------------------------------------------------------------------------------------------
# Set variables depending on dataset
case $DATA in
   ngc3028)
    VERSION="NextGems Cycle 3 ($DATA); $(date)"
    REFERENCE="https://nextgems-h2020.eu/"
    ;;
  *     )
    echo "*** ERROR: Unknown dataset $DATA" && exit 1 ;;
esac

# -----------------------------------------------------------------------------------------------------
# Write parameter table
PARTAB="$(mktemp -u --tmpdir=.)_partab.txt"
cat > $PARTAB << EOF
&parameter
  name = longwave
  standard_name = surface_downwelling_longwave_flux_in_air
  long_name = "Surface Downwelling Longwave Radiation"
  units = "W m-2"
/
&parameter
  name = shortwave
  standard_name = surface_downwelling_shortwave_flux_in_air
  long_name = "Surface Downwelling Shortwave Radiation"
  units = "W m-2"
/
&parameter
  name = precip
  standard_name = precipitation_flux
  long_name = "Precipitation"
  units = "kg m-2 s-1"
/
&parameter
  name = wspeed
  standard_name = wind_speed
  long_name = "Wind Speed at lowest model level"
  units = "m s-1"
/
&parameter
  name = tmin
  standard_name = air_temperature
  long_name = "Daily Minimum Air Temperature at lowest model level"
  units = "degC"
/
&parameter
  name = tmax
  standard_name = air_temperature
  long_name = "Daily Maximum Air Temperature at lowest model level"
  units = "degC"
/
&parameter
  name = air_temp
  standard_name = air_temperature
  long_name = "Air Temperature at lowest model level"
  units = "degC"
/
&parameter
  name = qair
  standard_name = specific_humidity
  long_name = "Specific Humidity at lowest model level"
  units = "kg kg-1"
/
EOF

# -----------------------------------------------------------------------------------------------------
# Get path to data at required resolutions
DATAPATH_DEF=$(query_yaml.py ICON ${DATA} -s time=${STEP} zoom=${ZOOM} --cdo)
case $STEP in
  P1D ) DATAPATH_TAS=$(query_yaml.py ICON ${DATA} -s time=PT3H zoom=${ZOOM} --cdo);;
  * )   DATAPATH_TAS=DATAPATH_DEF;;
esac
echo "Found data at ${DATAPATH_DEF}"

WEIGHTS="$(mktemp -u --tmpdir=.)_weights.nc"
cdo -s gendis,${GRID},12 $DATAPATH_DEF $WEIGHTS

# -----------------------------------------------------------------------------------------------------
# Loop over years and download data
YEARLIST=$(seq ${YEAR%-*} ${YEAR#*-})
for Y in $YEARLIST ; do
  temp1=$(mktemp -u --tmpdir=.)
  OUT=`echo climate_${DATA}_${STEP}_${Y}.nc | tr [A-Z] [a-z]`
  unset MONFILES
  # Loop over months (otherwise too much memory is needed and the job will be killed)
  for M in $(seq 1 12); do
    echo "Processing $OUT, Month $M"
    THISMONTH="climate_month_${Y}-${M}.nc"
    if [ ! -f $THISMONTH ]; then
      # Get, process and remap surface variables
      $cdo remap,${GRID},${WEIGHTS} -expr,"longwave=rlds;shortwave=rsds;precip=pr;" \
        -select,name=pr,rlds,rsds,year=$Y,month=$M $DATAPATH_DEF ${temp1}_srf.nc
#      cdo settaxis,$Y-$M-01 ${temp1}_srf1.nc ${temp1}_srf.nc
      # Get, process and remap variables from lowest model level
      $cdo remap,${GRID},${WEIGHTS} -expr,"qair=hus;wspeed=sqrt(sqr(va) + sqr(ua));" \
        -select,name=hus,va,ua,year=$Y,month=$M,level=90 $DATAPATH_DEF ${temp1}_lowlev.nc
#      cdo settaxis,$Y-$M-01 ${temp1}_lowlev1.nc ${temp1}_lowlev.nc
      # Get, process and remap lowest model level temperature
      if [ $STEP == "P1D" ]; then
        # Compute daily min and max temperature
        cdo -s -f nc -setname,tmin -subc,273.15 -daymin -select,name=ta,year=$Y,month=$M,level=90 $DATAPATH_TAS ${temp1}_tmin.nc
#        cdo settaxis,$Y-$M-01 ${temp1}_tmin1.nc ${temp1}_tmin.nc
        cdo -s -f nc -setname,tmax -subc,273.15 -daymax -select,name=ta,year=$Y,month=$M,level=90 $DATAPATH_TAS ${temp1}_tmax.nc
#        cdo settaxis,$Y-$M-01 ${temp1}_tmax1.nc ${temp1}_tmax.nc
        $cdo remap,${GRID},${WEIGHTS} -merge ${temp1}_tmin.nc ${temp1}_tmax.nc ${temp1}_ta.nc && \
          rm ${temp1}_tmin.nc ${temp1}_tmax.nc
      else
        # use temperature at given temporal time step
        $cdo remap,${GRID},${WEIGHTS} -subc,273.15 -expr,"air_temp=ta;" \
          -select,name=ta,year=$Y,month=$M,level=90 $DATAPATH_DEF ${temp1}_ta.nc
#        cdo settaxis,$Y-$M-01 ${temp1}_ta1.nc ${temp1}_ta.nc
      fi

      # Merge variables into monthly file
      $cdo -a -b F64 setpartabn,${PARTAB} ${RW_TIME/YYYY-MM/$Y-$M} -merge ${temp1}_srf.nc ${temp1}_lowlev.nc ${temp1}_ta.nc $THISMONTH \
        && rm ${temp1}_srf.nc ${temp1}_lowlev.nc ${temp1}_ta.nc
    fi
    MONFILES="$MONFILES $THISMONTH"
  done

  # Merge monthly files into yearly file
  $cdo mergetime $MONFILES $OUT && rm $MONFILES

  # Write File metainfo
  ncatted -h -a title,global,o,c,"ICON-LAND standalone forcing based on $DATA" $OUT
  ncatted -h -a institution,global,o,c,"${INSTITUTE}"                          $OUT
  ncatted -h -a contact,global,o,c,"${CONTACT}"                                $OUT
  ncatted -h -a version,global,o,c,"${VERSION}"                                $OUT
  ncatted -h -a reference,global,o,c,"${REFERENCE}"                            $OUT
  ncatted -h -a comment,global,d,c,"${COMMENT}"                                $OUT
  ncatted -h -a history,global,d,c,""                                          $OUT

  # Move to target directory
  mv $OUT ${DTRG}/
done

test -e $PARTAB && rm $PARTAB
test -e $WEIGHTS && rm $WEIGHTS
