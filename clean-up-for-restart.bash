#!/bin/bash -x

end_year=`expr $1 + 3`

backup_dir=$1-${end_year}
mkdir ./${backup_dir}
mv ./*_lnd_basic_ml_*Z.nc ./${backup_dir}
mv ./*_restart_jsbalone_202[1-5]*T*Z.nc ./${backup_dir}

touch isRestartRun.sem
cdo settaxis,2021-01-01 ${backup_dir}/e40_jsbalone_R2B4_EU_restart_jsbalone_20250101T000000Z.nc ./e40_jsbalone_R2B4_EU_restart_jsbalone_20210101T000000Z_temp.nc
cdo setattribute,tc_startdate="2021-01-01T00:00:00.000" e40_jsbalone_R2B4_EU_restart_jsbalone_20210101T000000Z_temp.nc e40_jsbalone_R2B4_EU_restart_jsbalone_20210101T000000Z.nc
mv e40_jsbalone_R2B4_EU_restart_jsbalone_20210101T000000Z.nc ./${backup_dir}/e40_jsbalone_R2B4_EU_restart_jsbalone_20250101T000000Z.nc_time_corrected_to_20210101
rm ./e40_jsbalone_R2B4_EU_restart_jsbalone_20210101T000000Z_temp.nc ./restart_jsbach_DOM01.nc
ln -sf ./${backup_dir}/e40_jsbalone_R2B4_EU_restart_jsbalone_20250101T000000Z.nc_time_corrected_to_20210101 ./restart_jsbach_DOM01.nc
