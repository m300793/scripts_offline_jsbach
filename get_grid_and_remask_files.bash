#!/bin/bash -x

grid=9

if [ ${grid} -eq 9 ]
then
grid_path=/pool/data/ICON/grids/public/mpim/0015
land_path=/pool/data/ICON/grids/public/mpim/0015-0016/land/r0007

ln -sf ${grid_path}/icon_grid_0015_R02B09_G.nc .

cdo -setname,lnd_cells -gtc,-0.1 -selname,MASK ${land_path}/hdpara_r2b9_0015_0016_mc_s_v1.nc regmask_0015_R02B09_land.nc

ncap2 -s 'where(clon>=5*3.14/180 && clon<=15*3.14/180 && clat>=45*3.14/180 && clat<=55*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0015_R02B09_land.nc regmask_0015_R02B09_germany.nc

ncap2 -s 'where(clon>=-11*3.14/180 && clon<=34.5*3.14/180 && clat>=35*3.14/180 && clat<=72*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0015_R02B09_land.nc regmask_0015_R02B09_EU.nc
fi

if [ ${grid} -eq 4 ]
then
grid_path=/pool/data/ICON/grids/public/mpim/0043
#land_path=/work/bm1236/m214103/Data/JSB_BCIC/ExtparTest/0043/land/r00xx

ln -sf ${grid_path}/icon_grid_0043_R02B04_G.nc .

cdo -setname,lnd_cells -gtc,-0.1 -selname,MASK /pool/data/ICON/grids/public/mpim/0043/land/r0004/hdpara_r2b4_0043_no_sinks.nc regmask_0043_R02B04_land.nc

ncap2 -s 'where(clon>=5*3.14/180 && clon<=15*3.14/180 && clat>=45*3.14/180 && clat<=55*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0043_R02B04_land.nc regmask_0043_R02B04_germany.nc

ncap2 -s 'where(clon>=-11*3.14/180 && clon<=34.5*3.14/180 && clat>=35*3.14/180 && clat<=72*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0043_R02B04_land.nc regmask_0043_R02B04_EU.nc
fi

if [ ${grid} -eq 6 ]
then
grid_path=/pool/data/ICON/grids/public/mpim/0021
#land_path=/work/bm1236/m214103/Data/JSB_BCIC/ExtparTest/0021/land/r00xx

ln -sf ${grid_path}/icon_grid_0021_R02B06_G.nc .

cdo -setname,lnd_cells -gtc,-0.1 -selname,MASK /pool/data/ICON/grids/private/dian/mpim/0021/HD/hdpara_r2b6_0021_0035_no_sinks.nc regmask_0021_R02B06_land.nc

ncap2 -s 'where(clon>=5*3.14/180 && clon<=15*3.14/180 && clat>=45*3.14/180 && clat<=55*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0021_R02B06_land.nc regmask_0021_R02B06_germany.nc

ncap2 -s 'where(clon>=-11*3.14/180 && clon<=34.5*3.14/180 && clat>=35*3.14/180 && clat<=72*3.15/180) lnd_cells=lnd_cells; elsewhere lnd_cells=0' regmask_0021_R02B06_land.nc regmask_0021_R02B06_EU.nc
fi
