#!/bin/bash -x

grid=4

grid_path=/work/mh0731/m300793/grid-generator2/grids
land_path=/pool/data/ICON/grids/public/mpim/0015-0016/land/r0007


if [ ${grid} -eq 9 ]
then

run_path=./R02B09/0015
mkdir -p ${run_path}
cd ${run_path}

ln -sf ${grid_path}/icon_grid_0015_R02B09_EU.nc .

cdo remapnn,icon_grid_0015_R02B09_EU.nc ${land_path}/ic_land_soil_2015.nc ic_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0015_R02B09_EU.nc ${land_path}/bc_land_sso_2015.nc bc_land_sso_2015_EU.nc
cdo remapnn,icon_grid_0015_R02B09_EU.nc ${land_path}/bc_land_soil_2015.nc bc_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0015_R02B09_EU.nc ${land_path}/bc_land_phys_2015.nc bc_land_phys_2015_EU.nc
cdo remapnn,icon_grid_0015_R02B09_EU.nc ${land_path}/bc_land_frac_11pfts_2015.nc bc_land_frac_11pfts_2015_EU.nc

fi


if [ ${grid} -eq 4 ]
then

run_path=./R02B04/0043
mkdir -p ${run_path}
cd ${run_path}

ln -sf ${grid_path}/icon_grid_0043_R02B04_EU.nc .

cdo remapnn,icon_grid_0043_R02B04_EU.nc ${land_path}/ic_land_soil_2015.nc ic_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0043_R02B04_EU.nc ${land_path}/bc_land_sso_2015.nc bc_land_sso_2015_EU.nc
cdo remapnn,icon_grid_0043_R02B04_EU.nc ${land_path}/bc_land_soil_2015.nc bc_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0043_R02B04_EU.nc ${land_path}/bc_land_phys_2015.nc bc_land_phys_2015_EU.nc
cdo remapnn,icon_grid_0043_R02B04_EU.nc ${land_path}/bc_land_frac_11pfts_2015.nc bc_land_frac_11pfts_2015_EU.nc

fi


if [ ${grid} -eq 6 ]
then

run_path=./R02B06/0021
mkdir -p ${run_path}
cd ${run_path}

ln -sf ${grid_path}/icon_grid_0021_R02B06_EU.nc .

cdo remapnn,icon_grid_0021_R02B06_EU.nc ${land_path}/ic_land_soil_2015.nc ic_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0021_R02B06_EU.nc ${land_path}/bc_land_sso_2015.nc bc_land_sso_2015_EU.nc
cdo remapnn,icon_grid_0021_R02B06_EU.nc ${land_path}/bc_land_soil_2015.nc bc_land_soil_2015_EU.nc
cdo remapnn,icon_grid_0021_R02B06_EU.nc ${land_path}/bc_land_phys_2015.nc bc_land_phys_2015_EU.nc
cdo remapnn,icon_grid_0021_R02B06_EU.nc ${land_path}/bc_land_frac_11pfts_2015.nc bc_land_frac_11pfts_2015_EU.nc

fi

