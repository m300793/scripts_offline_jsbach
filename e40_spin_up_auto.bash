#!/bin/bash

SECONDS=0

run_dir=/work/mh0731/m300793/Carbon/1jsb-hr-std/run/
exp_dir=/work/mh0731/m300793/Carbon/1jsb-hr-std/experiments/e40_jsbalone_R2B4_EU/


for yyyy in `seq 2025 4 2029`
do

cd ${run_dir}
echo "RUN STAR $((yyyy))! $((duration / 60)) minutes and $((duration % 60)) seconds elapsed." 
sbatch ./e40_jsbalone_R2B4_EU.run


  while [ ! -f ${exp_dir}/e40_jsbalone_R2B4_EU_lnd_basic_ml_20250101T000000Z.nc ]
  do
    sleep 5
    duration=$SECONDS
    echo "waiting... $((duration / 60)) minutes and $((duration % 60)) seconds elapsed."
  done
echo "RUN IS DONE for $((yyyy))! $((duration / 60)) minutes and $((duration % 60)) seconds elapsed." 
sleep 5

jobid=`squeue -u $USER --format="%.18i %.9P %.30j %.4t %.10M %.6D %R" | grep -i exp.e40_jsbalone | grep R | awk ' { print $1 }'`
scancel ${jobid}

cd ${exp_dir}
./clean-up-for-restart.bash ${yyyy}
echo "CLEAN-UP IS DONE for $((yyyy))! $((duration / 60)) minutes and $((duration % 60)) seconds elapsed." 
echo " "
echo " "
  
done

echo "!###############################!"
echo "JOBS ARE DONE. It took for $((yyyy))! $((duration / 60)) minutes and $((duration % 60)) seconds elapsed." 
