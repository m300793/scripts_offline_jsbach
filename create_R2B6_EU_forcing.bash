#!/bin/bash -x

grid_path=/work/mh0731/m300793/Carbon/grid-generator/grids
base_forcing_path=/work/mh0731/m300793/Carbon/forcing/global/R02B09/0015/NGC3028


ln -sf ${grid_path}/icon_grid_0021_R02B06_EU.nc .

for yyyy in `seq 2021 1 2024` 
do

  cdo remapnn,icon_grid_0021_R02B06_EU.nc ${base_forcing_path}/climate_ngc3028_p1d_${yyyy}.nc climate_ngc3028_p1d_${yyyy}.nc

done
